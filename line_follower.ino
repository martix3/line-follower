#include "line_follower.hpp"

namespace {
AAR_04::LineFollower follower{100};
}

void setup()
{
  follower.setup();
  delay(2000);
}

void loop()
{
  follower.update();
}
