#include "aar04_pin_cfg.hpp"

namespace AAR_04 {

class LineFollower
{
  public:
    LineFollower(const unsigned int steering_sensitivity)
      : m_SteeringSensitivity(steering_sensitivity)
    {
    }

    void setup()
    {
      pinMode(PIN_MOTOR_LEFT_BACKWARD, OUTPUT);
      pinMode(PIN_MOTOR_LEFT_FORWARD, OUTPUT);
      pinMode(PIN_MOTOR_RIGHT_FORWARD, OUTPUT);
      pinMode(PIN_MOTOR_RIGHT_BACKWARD, OUTPUT);

      pinMode(PIN_LINE_TRACKING_LED, OUTPUT);
      digitalWrite(PIN_LINE_TRACKING_LED, HIGH);

      digitalWrite(PIN_MOTOR_LEFT_BACKWARD, LOW);
      digitalWrite(PIN_MOTOR_RIGHT_BACKWARD, LOW);

      analogWrite(PIN_MOTOR_LEFT_FORWARD, 0);
      analogWrite(PIN_MOTOR_RIGHT_FORWARD, 0);
    }

    void update()
    {
      const auto line_position = analogRead(PIN_LINE_TRACKING_SENSOR_LEFT) - analogRead(PIN_LINE_TRACKING_SENSOR_RIGHT);
      
      if (line_position < m_SteeringSensitivity) {
        right();
      } else if (line_position < -m_SteeringSensitivity) {
        left();
      } else {
        straight();
      }
    }


  private:

    void left() {
      analogWrite(PIN_MOTOR_LEFT_FORWARD, 120);
      analogWrite(PIN_MOTOR_RIGHT_FORWARD, 90);
    }


    void right() {
      analogWrite(PIN_MOTOR_LEFT_FORWARD, 90);
      analogWrite(PIN_MOTOR_RIGHT_FORWARD, 120);
    }


    void straight()
    {
      analogWrite(PIN_MOTOR_LEFT_FORWARD, 120);
      analogWrite(PIN_MOTOR_RIGHT_FORWARD, 120);
    }


    const unsigned int m_SteeringSensitivity;
};

}
