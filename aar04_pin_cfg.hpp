namespace AAR_04 {

// Serial baudrate
const unsigned int SERIAL_BAUDRATE = 9600;

// Motor pins
const unsigned int  PIN_MOTOR_RIGHT_FORWARD  =  5;
const unsigned int  PIN_MOTOR_RIGHT_BACKWARD =  6;
const unsigned int  PIN_MOTOR_LEFT_FORWARD   =  9;
const unsigned int  PIN_MOTOR_LEFT_BACKWARD  =  10;

// Line tracking LED pins
const unsigned int  PIN_LINE_TRACKING_SENSOR_LEFT = A6;
const unsigned int  PIN_LINE_TRACKING_SENSOR_RIGHT = A7;

// Led between the line tracing sensors
const unsigned int  PIN_LINE_TRACKING_LED = 7

}
